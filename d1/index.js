console.log("Hello World");

//SECTION - JSON Objects
/*
	-JSON stands for JavaScript Object Notation
	-JSON is also used in other programming language, hence the "Notation" in it's name
	-JSON is not to be confused with JavaScript objects
*/

//Mini-Activity
//below is an example of JS object
/*let cities = [
{
city: "Calamba",
province: "Laguna",
country: "Philippines",
},
{
city: "Palo Alto",
province: "Laguna",
country: "Philippines",
},
{
city: "Cabuyao",
province: "Laguna",
country: "Philippines",
}]
*/


//below is an example of JSON Array
/*
	WHAT JSON DOES
		-JSON is used for serializing different data types into bytes

		SYNTAX:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/
let cities = [
{
"city": "Calamba",
"province": "Laguna",
"country": "Philippines",
},
{
"city": "Palo Alto",
"province": "Laguna",
"country": "Philippines",
},
{
"city": "Cabuyao",
"province": "Laguna",
"country": "Philippines",
}]
console.log(cities);	

//JSON Methods



//SECTION - stringify method


let batchesArr = [ {batchName: "Batch X"},{batchName: "Batch Y"}];
console.log(batchesArr);

//stringify method - used to convert JS object into a string
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));


//Miniactivity

/*let data = {
	name: "Arthur",
	age: 30,
	address: {
		city: "Calamba",
		country: "Philippines",
	},
}
console.log(JSON.stringify(data));*/

//direct conversion of data type from JS object into stringify 

let data = JSON.stringify({
	name: "Arthur",
	age: 30,
	address: {
		city: "Calamba",
		country: "Philippines",
	},
});
console.log(data);

//an example where JSON is commonly used is on package.json files which an express JS app uses to keep track of the info regarding a repository/project (see package.json)
/*{
    "name": "javascript-server",
    "version": "1.0.0",
    "description": "Boilerplate server application.",
    "main": "index.js",
    "scripts": {
          "test": "echo \"Error: no test specified\" && exit 1",
    },
    "keywords": [],
    "author": "John Smith",
    "license": "ISC"
}*/

//JSON.stringify with prompt()

/*
		Syntax:
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB,
		})

		since we do not have front end application yet , we will use the prompt method in order to gather user data.
*/

let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age");
let address = {
	city: prompt("Which city do you live?"),
	country: prompt("Which country does your city belong?"),
}
let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address
});

console.log("Result from stringify method");
console.log(otherData);


//SECTION - parse method - convert string to object

let batchesJSON = `[{"batchName": "BatchX"},{"batchName": "BatchY"}]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));


//mini activity


console.log("Result from parse method");
console.log(JSON.parse(data));
console.log(JSON.parse(otherData));